**Securing a RDBS with Vault**


1. Create a Docker network for your development environment named ``vault-demo``

```
docker network create vault-demo
```

2. Start a MariaDB container on your new network named ``app-db``

```
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root-password -e \
 MYSQL_DATABASE=demo --network vault-demo --name app-db mariadb:latest
```

3. Start a Vault container named ``vault`` in development mode and start a shell session 

```
docker exec -it $(docker run --cap-add=IPC_LOCK -d -p 8200:8200 -e \
 'VAULT_DEV_ROOT_TOKEN_ID=vault-token' -e VAULT_ADDR='http://127.0.0.1:8200' \
 --network vault-demo --name=vault vault:latest) /bin/sh
```

4. Verify that the Vault server started successfully

```
vault status
```

5. Authenticate your session using the value set to ``VAULT_DEV_ROOT_TOKEN_ID`` above

```
vault login
```

6. Enable file-based audit device

```
vault audit enable file file_path=/vault/logs/vault_audit.log
```

7. Enable database secrets engine

```
vault secrets enable database
```

8. Configure MySQL/MariaDB plugin and add connection details

```
vault write database/config/demo \
 plugin_name=mysql-database-plugin \
 connection_url="{{username}}:{{password}}@tcp(app-db:3306)/" \
 allowed_roles="admin" \
 username="root" \
 password="root-password"
```

9. Configure ``admin`` database role 

```
vault write database/roles/admin \
 db_name=demo \
 creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}'; \
 GRANT ALL PRIVILEGES ON demo.* TO '{{name}}'@'%';" \
 default_ttl="1h" \
 max_ttl="24h"
```

10. Generate a new credential with the ``admin`` role

```
vault read database/creds/admin
```

11. Generate a token for instances of an application named ``demo``

```
vault token create -metadata=app-name=demo
```

12. Add the generated token to an environment variable named ``VAULT_TOKEN`` in your IDE's run configurations and start the application


**Use Vault Key/Value Secrets Engine for Application Configuration**

1. Enable the Vault K/V Secrets Engine

```
vault secrets enable kv
```

2. Add a configuration property named ``config.value`` to the key-value store

```
vault kv put secret/demo-app config.value=abc123
```

3. Create a class named ``Configuration`` to store your property

```java
package com.example.app;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("config")
public class Configuration {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
```

4. Enable configuration properties and inject the value into your Application

```java
package com.example.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(Configuration.class)
public class Application {

	private final Configuration configuration;

	public Application(Configuration configuration) {
		this.configuration = configuration;
		System.out.println("value: " + configuration.getValue());
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
```

5. Enable Spring Vault KV

```
spring:
  application:
    name: demo-app
  cloud:
    vault:
      token: ${VAULT_TOKEN}
      scheme: http
      host: localhost
      port: 8200
      database:
        enabled: true
        role: admin
        backend: database
        username-property: spring.datasource.username
        password-property: spring.datasource.password
      kv:
        enabled: true
```